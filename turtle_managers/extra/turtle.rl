type { 
    Position
    float
}

skillset turtle {
  data {
    position: Position
  }
  function {
    at(p: Position): bool
    can_dive(p: Position): bool
  }
  resource {
    turtle_status {
      initial ON_SURFACE
      ON_SURFACE -> IN_WATER
      IN_WATER -> ON_SURFACE
    }
    sensor {
      initial OFF
      OFF -> ON
      ON -> OFF
      extern ON -> OUT_OF_SERVICE
      extern OFF -> OUT_OF_SERVICE
    }
    autonomous_control {
      initial ON
      extern ON -> OFF
      extern OFF -> ON
    }
    control_mode {
      initial IDLE
      IDLE -> MOVING
      MOVING -> IDLE
    }
  }
  skill {
    dive {
      progress = 1.0
      effect {
        in_water {
          turtle_status -> IN_WATER
          control_mode -> IDLE
        }
        moving {
          control_mode -> MOVING
        }
        idle {
          control_mode -> IDLE
        }
      }
      precondition {
        depth_ok {
          (at(position) && can_dive(position))
        }
        on_surface_ready {
          resource ((turtle_status == ON_SURFACE) && (control_mode == IDLE))
        }
        autonomous {
          resource (autonomous_control == ON)
        }
        success moving
      }
      invariant {
        autonomous{
          resource  (autonomous_control == ON)
          violation idle
        }
      }
      mode {
        SUCCESS {
          apply   in_water
        }
        FAILURE {
          apply   idle
        }
      }
    }
    ascend {
      progress = 1.0
      effect {
        on_surface {
          turtle_status -> ON_SURFACE
          control_mode -> IDLE
        }
        moving {
          control_mode -> MOVING
        }
        idle {
          control_mode -> IDLE
        }
      }
      precondition {
        in_water_ready {
          resource ((turtle_status == IN_WATER) && (control_mode == IDLE))
        }
        autonomous {
          resource (autonomous_control == ON)
        }
        success moving
      }
      invariant {
        autonomous{
          resource  (autonomous_control == ON)
          violation idle
        }
      }
      mode {
        SUCCESS {
          apply   on_surface
        }
        FAILURE {
          apply   idle
        }
      }
    }
    move_on_surface {
      progress = 1.0
      input {
        target: Position
      }
      effect {
        moving {
          control_mode -> MOVING
        }
        idle {
          control_mode -> IDLE
        }
      }
      precondition {
        on_surface {
          resource ((turtle_status == ON_SURFACE) && (control_mode == IDLE))
        }
        current_position {
          at(position)
        }
        autonomous {
          resource (autonomous_control == ON)
        }
        success moving
      }
      invariant {
        on_surface{
          resource  (robot_status == ON_SURFACE)
          violation idle
        }
        autonomous{
          resource  (autonomous_control == ON)
          violation idle
        }
      }
      mode {
        ARRIVED {
          ((! at(position)) && at(target))
          apply   idle
        }
        ABORTED {
          apply   idle
        }
      }
    }
    move {
      progress = 1.0
      input {
        target: Position
      }
      effect {
        moving {
          control_mode -> MOVING
        }
        idle {
          control_mode -> IDLE
        }
      }
      precondition {
        in_water {
          resource ((turtle_status == IN_WATER) && (control_mode == IDLE))
        }
        current_position {
          at(position)
        }
        autonomous {
          resource (autonomous_control == ON)
        }
        success moving
      }
      invariant {
        in_water{
          resource  (robot_status == IN_WATER)
          violation idle
        }
        autonomous{
          resource  (autonomous_control == ON)
          violation idle
        }
      }
      mode {
        ARRIVED {
          ((! at(position)) && at(target))
          apply   idle
        }
        ABORTED {
          apply   idle
        }
      }
    }
    turn_sensor_on {
      progress = 1.0
      effect {
        sensor_on {
          sensor -> ON
        }
      }
      precondition {
        sensor_off {
          resource (sensor == OFF)
        }
      }
      mode {
        ON {
          apply   sensor_on
        }
      }
    }
    turn_sensor_off {
      progress = 1.0
      effect {
        sensor_off {
          sensor -> OFF
        }
      }
      precondition {
        sensor_on {
          resource (sensor == ON)
        }
      }
      mode {
        OFF {
          apply   sensor_off
        }
      }
    }
    search {
      progress = 1.0
      input {
        timeout: float
      }
      effect {
        moving {
          control_mode -> MOVING
        }
        idle {
          control_mode -> IDLE
        }
      }
      precondition {
        in_water {
          resource ((turtle_status == IN_WATER) && (control_mode == IDLE))
        }
        sensor_on {
          resource (sensor == ON)
        }
        autonomous {
          resource (autonomous_control == ON)
        }
        success moving
      }
      invariant {
        autonomous{
          resource  (autonomous_control == ON)
          violation idle
        }
      }
      mode {
        FOUND {
          apply   idle
        }
        TIMEDOUT {
          apply   idle
        }
      }
    }
  }
}

