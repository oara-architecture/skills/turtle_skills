import launch
import launch.actions
import launch.substitutions
import launch_ros.actions
import launch.conditions

def generate_launch_description():
    ns = launch.actions.DeclareLaunchArgument(
        'ns',
        default_value='/',
        description='Namespace in which to launch nodes')
    
    
    res_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='turtle_resource_manager',
        name='turtle_resource_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    
    data_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='turtle_data_manager',
        name='turtle_data_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    
    dive_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='dive_manager.py',
        name='dive_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    ascend_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='ascend_manager.py',
        name='ascend_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    move_on_surface_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='move_on_surface_manager.py',
        name='move_on_surface_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    move_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='move_manager.py',
        name='move_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    turn_sensor_on_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='turn_sensor_on_manager.py',
        name='turn_sensor_on_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    turn_sensor_off_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='turn_sensor_off_manager.py',
        name='turn_sensor_off_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    
    search_manager = launch_ros.actions.Node(
        package='turtle_managers',
        executable='search_manager.py',
        name='search_manager',
        output="log",
        namespace=launch.substitutions.LaunchConfiguration('ns'))
    

    return launch.LaunchDescription([ns,
        res_manager,
        data_manager,
        dive_manager,ascend_manager,move_on_surface_manager,move_manager,turn_sensor_on_manager,turn_sensor_off_manager,search_manager,
    ])
