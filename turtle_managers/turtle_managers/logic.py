class Formula:
    def __init__(self, left, right):
        self._left = left
        self._right = right

    def symbols(self):
        return self._left.symbols() | self._right.symbols()

    def eval(self, symbols):
        return False


class And(Formula):
    def eval(self, symbols):
        return self._left.eval(symbols) and self._right.eval(symbols)


class Or(Formula):
    def eval(self, symbols):
        return self._left.eval(symbols) or self._right.eval(symbols)


class Implies(Formula):
    def eval(self, symbols):
        if self._left.eval(symbols):
            return self._right.eval(symbols)
        return True


class Equals(Formula):
    def symbols(self):
        return {self._left}

    def eval(self, symbols):
        return self._right == symbols[self._left]


class Diff(Formula):
    def symbols(self):
        return {self._left}

    def eval(self, symbols):
        return self._right != symbols[self._left]
