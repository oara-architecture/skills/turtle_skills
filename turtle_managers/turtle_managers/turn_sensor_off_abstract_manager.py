from abc import ABC, abstractmethod
from functools import partial
import enum
import threading
import math

import rclpy
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node
from rclpy.action import ActionServer
from rclpy.action.server import GoalResponse, GoalStatus, CancelResponse

from std_msgs.msg import String
from std_srvs.srv import Empty
from diagnostic_msgs.srv import SelfTest
from diagnostic_msgs.msg import DiagnosticStatus, KeyValue

from turtle_managers.action import TurnSensorOffSkill
from turtle_managers.srv import State, Transition
from .logic import *

@enum.unique
class SkillTurn_sensor_offGoalState(enum.Enum):
    S = 'Started'
    NV = 'Not Valid'
    CR = 'Checking Resources'
    NR = 'Resources Preconditions Not Matched'
    RG = 'Running'
    IG = 'Interrupting'
    RI = 'Resource Invariant Violated'
    T_OFF = 'Terminating OFF'
    M_OFF = 'Terminated in OFF'
    N_OFF = 'Postcondition failure in OFF'

    @classmethod
    def is_terminal(cls, state):
        if state == cls.NV: return True
        if state == cls.NR: return True
        if state == cls.RI: return True
        if state == cls.M_OFF: return True
        if state == cls.N_OFF: return True
        return False

class AbstractTurnSensorOffSkillManager(ABC, Node):

    def __init__(self):
        Node.__init__(self, 'turn_sensor_off_manager')
        self.__action_callback_group = ReentrantCallbackGroup()
        self.__resource_callback_group = ReentrantCallbackGroup()
        self.__executor = MultiThreadedExecutor()

        self.__goals = dict()
        self.__goals_msgs = dict()
        self.__goals_states = dict()
        self.__goals_progress = dict()
        self.__goals_start_time = dict()
        self.__goals_end_time = dict()
        self.__modes = dict()
        self.__events = dict()

        self.__as = ActionServer(self,
            TurnSensorOffSkill,
            'skill/turn_sensor_off',
            self.__exec_cb,
            goal_callback=self.__goal_cb,
            handle_accepted_callback=self.__accepted_cb,
            cancel_callback=self.__cancel_cb,
            callback_group=self.__action_callback_group
            )

        
        self.__resource_lock = self.create_client(Empty,
            'resource/internal/lock',
            callback_group=self.__resource_callback_group)
        self.__resource_unlock = self.create_client(Empty,
            'resource/internal/unlock',
            callback_group=self.__resource_callback_group)
        self.get_logger().info("waiting connection to resource manager")
        self.__resource_lock.wait_for_service()
        self.__resource_transition = dict()
        self.__resource_state = dict()
        self.__resource_sub = dict()
        
        self.get_logger().info("create client for resource manager sensor")
        self.__resource_transition['sensor'] = self.create_client(Transition,
            'resource/internal/change_sensor',
            callback_group=self.__resource_callback_group)
        self.__resource_state['sensor'] = self.create_client(State,
            'resource/get_sensor',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['sensor'] = self.create_subscription(String,
            'resource/sensor_state',
            self.__invariants_cb,
            10)
        
        

        self.__formula_pre = dict()
        
        self.__formula_pre['sensor_on'] = Equals('sensor', 'ON')
        
        self.__formula_modes = dict()
        
        self.__formula_inv = dict()
        

        self.__status_service = self.create_service(SelfTest,
            'skill/turn_sensor_off/get_status',
            self.get_status,
            callback_group=self.__action_callback_group)

        self.__progress_period = 1.0
        
        self.__timer = self.create_timer(1.0, self.publish_feedbacks,
                                         callback_group=self.__action_callback_group)
        
        self.get_logger().info("skill manager started")

    def __id(self, gh):
        return str(gh.goal_id.uuid)

    @property
    def progress_period(self):
        return self.__progress_period

    def spin(self):
        rclpy.spin(self, self.__executor)

    def spin_until_future_complete(self, future):
        return self.__executor.spin_until_future_complete(future)

    def get_status(self, request, response):
        response.status = []
        response.status.append(DiagnosticStatus(level=DiagnosticStatus.OK,
            name='turn_sensor_off',
            message='turtle_managers.action.TurnSensorOffSkill'))
        for gid, st in self.__goals_states.items():
            status = DiagnosticStatus(name='turn_sensor_off')
            if gid in self.__goals:
                status.level = DiagnosticStatus.OK
                status.values.append(KeyValue(key='server_state', value=str(self.__goals[gid].status)))
                #status.values.append(KeyValue(key='server_state_text', value=self.__goals[gid].get_goal_status().text))
            else:
                status.level = DiagnosticStatus.STALE
            status.hardware_id = gid
            status.values.append(KeyValue(key='start_time', value=str(self.__goals_start_time[gid])))
            status.values.append(KeyValue(key='end_time', value=str(self.__goals_end_time[gid])))            
            status.values.append(KeyValue(key='goal', value=repr(self.__goals_msgs[gid])))
            status.values.append(KeyValue(key='skill_state', value=st.name))
            status.values.append(KeyValue(key='skill_state_text', value=st.value))
            status.values.append(KeyValue(key='progress', value=str(self.__goals_progress[gid])))
            response.status.append(status)
        response.id = 'turn_sensor_off'
        response.passed = DiagnosticStatus.OK
        return response

    def check_pre(self):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                future = self.__resource_state[res].call_async(State.Request())
                self.__executor.spin_until_future_complete(future)
                self.get_logger().debug("received response from resource manager")
                states[res] = future.result().state
            if not formula.eval(states):
                self.get_logger().error("precondition {} error".format(label))
                return False, label
        return True, ''

    
    def __effect_sensor_off(self):
        self.on_effect_sensor_off()
        
        future = self.__resource_transition['sensor'].call_async(Transition.Request(
            target='OFF'))
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("received response from resource manager")
        r = future.result()
        if not r.success:
            self.get_logger().error("error changing resource sensor state: {} -> OFF".format(r.source))
            return False, 'sensor'
        
        return True, ''
    

    
    def __resource_manager_lock(self):
        self.get_logger().info("locking resource manager")
        future = self.__resource_lock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager locked")
        
    def __resource_manager_unlock(self):
        self.get_logger().info("unlocking resource manager")
        future = self.__resource_unlock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager unlocked")
    

    def __invariants_cb(self, msg):
        active_goals = [gid for gid in self.__goals
                        if (self.__goals_states[gid] == SkillTurn_sensor_offGoalState.RG
                            or self.__goals_states[gid] == SkillTurn_sensor_offGoalState.IG)]
        if not active_goals:
            return

        
        self.__resource_manager_lock()
        
        
        
        self.__resource_manager_unlock()
        

    def __goal_cb(self, goal_request):
        self.get_logger().info("validating goal {}".format(goal_request))
        #self.__goals_states[gid] = SkillTurn_sensor_offGoalState.S

        if self.validate(goal_request):
            self.get_logger().info("goal {} validated".format(goal_request))
            #self.__goals_states[gid] = SkillTurn_sensor_offGoalState.CR
            return GoalResponse.ACCEPT
        else:
            self.get_logger().warn("goal {} not validated".format(goal_request))
            #self.__goals_states[gid] = SkillTurn_sensor_offGoalState.NV
            return GoalResponse.REJECT # terminal state NV

    def __accepted_cb(self, gh):
        gid = self.__id(gh)
        self.__goals[gid] = gh
        self.__goals_msgs[gid] = gh.request
        self.__goals_progress[gid] = 0.0
        self.__goals_states[gid] = SkillTurn_sensor_offGoalState.CR
        self.__goals_start_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
        self.__goals_end_time[gid] = math.inf
        
        self.__resource_manager_lock()
        val, res = self.check_pre()
        if not val:
            self.__resource_manager_unlock()
            self.get_logger().warn("error in precondition {}".format(res))
            self.__modes[gid] = "NR({})".format(res)
            gh.execute()
            gh.abort()
            self.__goals_states[gid] = SkillTurn_sensor_offGoalState.NR
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return
        
        self.__resource_manager_unlock()
        
        self.__events[gid] = threading.Event()
        self.get_logger().info("dispatching goal {}".format(gh.goal_id))
        self.__goals_states[gid] = SkillTurn_sensor_offGoalState.RG
        self.__executor.create_task(self.on_dispatch, gid, gh.request)
        gh.execute()

    def __cancel_cb(self, gh):
        self.get_logger().info("interrupting goal {} {}".format(gh.goal_id, gh.request))
        self.__goals_states[self.__id(gh)] = SkillTurn_sensor_offGoalState.IG
        self.__executor.create_task(self.on_interrupt, self.__id(gh), gh.request)
        return CancelResponse.ACCEPT

    def __exec_cb(self, gh):
        gid = self.__id(gh)
        if gid not in self.__events:
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return TurnSensorOffSkill.Result(mode='NR')
        self.__events[gid].wait()
        status = self.__goals_states[gid]
        if not SkillTurn_sensor_offGoalState.is_terminal(status):
            self.get_logger().error("[goal {}: execution callback finished with status {}".format(gh.status, gid))
        else:
            mode = self.__modes[gid]
            self.get_logger().info("goal {}: returning mode {}".format(gid, mode))
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return TurnSensorOffSkill.Result(mode=mode)

    def publish_feedbacks(self):
        to_remove = []
        self.get_logger().debug("publishing progress")
        for gid, gh in self.__goals.items():
            status = self.__goals_states[gid]
            if status == SkillTurn_sensor_offGoalState.RG:
                progress = self.progress(gid, gh.request)
                self.__goals_progress[gid] = progress
                self.get_logger().debug(" - goal {} progress {}".format(gid, progress))
                gh.publish_feedback(TurnSensorOffSkill.Feedback(progress=progress))
            elif SkillTurn_sensor_offGoalState.is_terminal(status):
                to_remove.append(gid)
        for gid in to_remove:
            self.get_logger().info("removing terminated goal {}".format(gid))
            del self.__goals[gid]
            del self.__events[gid]

    
    def check_post_OFF(self):
        
        return True
        

    def terminated_OFF(self, gid):
        try:
            gh = self.__goals[gid]
        except KeyError:
            self.get_logger().warn("goal {} already purged".format(gid))
            return
        status = self.__goals_states[self.__id(gh)]
        if SkillTurn_sensor_offGoalState.is_terminal(status):
            self.get_logger().warn("goal {} already terminated in {}".format(gid, status))
            return
        
        if (status == SkillTurn_sensor_offGoalState.RG or status == SkillTurn_sensor_offGoalState.IG):
            self.get_logger().info("terminating goal {} {} in mode OFF".format(gid, gh.request))
            self.__goals_states[gid] = SkillTurn_sensor_offGoalState.T_OFF
            
            self.__resource_manager_lock()
            if not self.check_post_OFF():
                self.get_logger().error("mode OFF postcondition error")
                self.__modes[gid] = 'N_OFF'
                self.__goals_states[gid] = SkillTurn_sensor_offGoalState.N_OFF
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            val, label = self.__effect_sensor_off()
            if not val:
                self.get_logger().error("error applying effect sensor_off in mode OFF")
                self.__modes[gid] = 'N_OFF'
                self.__goals_states[gid] = SkillTurn_sensor_offGoalState.N_OFF
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            
            self.__resource_manager_unlock()
            
            self.get_logger().info("goal {} succeeded in mode OFF".format(gid))
            self.__modes[gid] = 'M_OFF'
            self.__goals_states[gid] = SkillTurn_sensor_offGoalState.M_OFF
            gh.succeed()
            self.__events[gid].set()
        else:
            self.get_logger().warn("goal {} not active ({})".format(gid, gh.status))
    

    ''' User functions must be reimplemented '''

    @abstractmethod
    def validate(self, goal_request):
        pass

    @abstractmethod
    def on_dispatch(self, goal_id, goal):
        pass

    @abstractmethod
    def on_interrupt(self, goal_id, goal):
        pass

    @abstractmethod
    def progress(self, goal_id, goal):
        pass

    
    @abstractmethod
    def on_effect_sensor_off(self):
        pass
    
