#!/usr/bin/env python3
import yaml
import threading

import rclpy
from rcl_interfaces.msg import ParameterDescriptor, ParameterType

from turtle_managers.turn_sensor_on_abstract_manager import AbstractTurnSensorOnSkillManager
from turtle_managers.srv import Transition

class AbstractTurnSensorOnSkillMockManager(AbstractTurnSensorOnSkillManager):

    def __init__(self):
        AbstractTurnSensorOnSkillManager.__init__(self)
        self.__progress = 0.0
        self.__delay = 1.0
        self.__sideeffect_event = threading.Event()

        self.__terminals = dict()
        
        self.__terminals['ON'] = self.terminated_ON
        

        self.declare_parameter("validate_delay", 1.0,
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE, 
                                                               description="sleep time for the validation step"))
        self.declare_parameter("dispatch_delay", 1.0,
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE, 
                                                               description="sleep time for the dispatching step; <0 do not terminate"))
        self.declare_parameter("interrupt_delay", 1.0,
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE, 
                                                               description="sleep time for the interruption step; <0 do not terminate"))
        self.declare_parameter("validate_result", True,
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_BOOL, 
                                                               description="result of the validation step"))
        self.declare_parameter("dispatch_mode", "ON",
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_STRING, 
                                                               description="termination mode after dispatching"))
        self.declare_parameter("interrupt_mode", "ON",
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_STRING, 
                                                               description="termination mode after interruption"))
        self.declare_parameter("dispatch_sideeffects", "{}",
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_STRING, 
                                                               description="side-effects on resources when dispatch is terminating, as a yaml dict 'resource'->'state'"))
        self.declare_parameter("interrupt_sideeffects", "{}",
                               descriptor=ParameterDescriptor(type=ParameterType.PARAMETER_STRING, 
                                                               description="side-effects on resources when interrupting, as a yaml dict 'resource'->'state'"))

    def sleep(self, delay):
        if delay > 0:
            rate = self.create_rate(1 / delay)
            rate.sleep()
            rate.destroy()

    def validate(self, goal):
        self.get_logger().info(f"validate goal {goal}")
        delay = self.get_parameter("validate_delay").value
        self.get_logger().debug(f"validation delay: {delay}")
        self.sleep(delay)
        self.__progress = 0.0
        result = self.get_parameter("validate_result").value
        self.get_logger().debug(f"validation result: {result}")
        return result

    def __sideeffect_cb(self, future):
        self.__sideeffect_event.set()

    def on_dispatch(self, goal_id, goal):
        self.get_logger().info(f"on_dispatch {goal}")
        self.__delay = self.get_parameter("dispatch_delay").value
        self.get_logger().debug(f"dispatching delay: {self.__delay}")
        if self.__delay > 0:
            self.sleep(self.__delay)
        if self.__delay < 0:
            return
        self.get_logger().info(f"terminating {goal}")
        sideeffects_yaml = self.get_parameter("dispatch_sideeffects").value
        self.get_logger().debug(f"side-effects parameter: {sideeffects_yaml}")
        sideffects = yaml.load(sideeffects_yaml, Loader=yaml.Loader)
        for resource, state in sideffects.items():
            self.get_logger().warning(f"applying effect on resource {resource} -> {state}")
            self.__sideeffect_event.clear()
            client = self.create_client(Transition, f"resource/change_{resource}")
            future = client.call_async(Transition.Request(target=state))
            future.add_done_callback(self.__sideeffect_cb)
            self.__sideeffect_event.wait()
        result = self.get_parameter("dispatch_mode").value
        self.get_logger().debug(f"dispatching terminal mode {result}")
        self.__terminals[result](goal_id)

    def on_interrupt(self, goal_id, goal):
        self.get_logger().info(f"on_interrupt {goal}")
        delay = self.get_parameter("interrupt_delay").value
        self.get_logger().debug(f"interruption delay: {delay}")
        if delay > 0:
            self.sleep(delay)
        if delay < 0:
            return
        self.get_logger().info(f"terminating {goal}")
        sideeffects_yaml = self.get_parameter("interrupt_sideeffects").value
        self.get_logger().debug(f"side-effects parameter: {sideeffects_yaml}")
        sideffects = yaml.load(sideeffects_yaml, Loader=yaml.Loader)
        for resource, state in sideffects.items():
            self.get_logger().warning(f"applying effect on resource {resource} -> {state}")
            self.__sideeffect_event.clear()
            client = self.create_client(Transition, f"resource/change_{resource}")
            future = client.call_async(Transition.Request(target=state))
            future.add_done_callback(self.__sideeffect_cb)
            self.__sideeffect_event.wait()
        result = self.get_parameter("interrupt_mode").value
        self.get_logger().debug(f"interruption terminal mode {result}")
        self.__terminals[result](goal_id)

    def progress(self, goal_id, goal):
        self.get_logger().info(f"progress {goal}: {self.__progress}")
        if self.__progress < self.__delay:
            self.__progress += self.progress_period / self.__delay
        return self.__progress
    
    
    def on_effect_sensor_on(self):
        self.get_logger().info("on_effect sensor_on")
    
