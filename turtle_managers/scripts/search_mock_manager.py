#!/usr/bin/env python3
import rclpy

from turtle_managers.search_abstract_mock import AbstractSearchSkillMockManager

class SearchSkillMockManager(AbstractSearchSkillMockManager):

    def __init__(self):
        AbstractSearchSkillMockManager.__init__(self)


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = SearchSkillMockManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
