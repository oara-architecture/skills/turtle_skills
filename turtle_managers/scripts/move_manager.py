#!/usr/bin/env python3
import rclpy

from turtle_managers.move_abstract_manager import AbstractMoveSkillManager

from turtlesim.msg import Pose
from std_msgs.msg import Float64
from std_srvs.srv import SetBool


class MoveSkillManager(AbstractMoveSkillManager):

    def __init__(self):
        AbstractMoveSkillManager.__init__(self)
        self.declare_parameter('tolerance', 0.15)
        self.__stop_client = self.create_client(SetBool, 'goto_controller/activate')
        self.__goal_pub = self.create_publisher(Pose, 'goal', 1)
        self.create_subscription(Float64, 'distance_to_goal', self.__distance_cb, 10)
        self.__init_dist = None
        self.__gid = None
        self.__progress = 0.0
    
    def __distance_cb(self, msg):
        if self.__gid is None: return
        if self.__init_dist is None:
            self.__init_dist = msg.data
        self.__progress = (self.__init_dist - msg.data) / self.__init_dist
        if msg.data < self.get_parameter('tolerance').value:
            self.get_logger().info(f"At {msg.data} of the goal")
            self.terminated_ARRIVED(self.__gid)

    def validate(self, goal):
        return self.__stop_client.wait_for_service(timeout_sec=1.0)

    def on_dispatch(self, goal_id, goal):
        self.__gid = goal_id
        self.get_logger().info(f"send goto goal {goal.target}")
        self.__goal_pub.publish(goal.target)

    def on_interrupt(self, goal_id, goal):
        self.__stop_client.call_async(SetBool.Request(data=False))
        self.terminated_ABORTED(goal_id)

    def progress(self, goal_id, goal):
        return self.__progress
    
    def on_effect_moving(self):
        pass

    def on_effect_idle(self):
        self.__gid = None


''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = MoveSkillManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
