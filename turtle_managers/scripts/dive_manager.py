#!/usr/bin/env python3
import rclpy

from turtle_managers.dive_abstract_manager import AbstractDiveSkillManager

from functools import partial
from turtlesim.msg import Color
from turtlesim.srv import SetPen


class DiveSkillManager(AbstractDiveSkillManager):

    def __init__(self):
        AbstractDiveSkillManager.__init__(self)
        self.create_subscription(Color, 'bgcolor', self.__bgcolor_cb, qos_profile=1)
        self.__client = self.create_client(SetPen, 'color_controller/fade_bg_color')
        self.declare_parameters('water_color', [('b', 250), ('g', 25), ('r', 25)])
        self.__color_goal = None
        self.__init_dist = None
        self.__progress = 0.0
    
    def validate(self, goal):
        return self.__client.wait_for_service(timeout_sec=1.0)

    def __bgcolor_cb(self, msg):
        if self.__color_goal is None:
            return
        self.get_logger().info(f"current color: {msg}")
        dist = max(abs(self.__color_goal.b - msg.b),
                   abs(self.__color_goal.g - msg.g),
                   abs(self.__color_goal.r - msg.r))
        if self.__init_dist is None:
            self.__init_dist = dist
            self.__progress = 0.0
        else:
            self.__progress = dist / self.__init_dist

    def __done_cb(self, goal_id, future):
        self.get_logger().info("In water")
        self.terminated_SUCCESS(goal_id)

    def on_dispatch(self, goal_id, goal):
        self.__progress = 0.0
        self.__color_goal = SetPen.Request(b=self.get_parameter('water_color.b').value,
                                           g=self.get_parameter('water_color.g').value,
                                           r=self.get_parameter('water_color.r').value)
        future = self.__client.call_async(request=self.__color_goal)
        self.get_logger().info(f"Start diving to {self.__color_goal}")
        future.add_done_callback(partial(self.__done_cb, goal_id))

    def on_interrupt(self, goal_id, goal):
        self.terminated_SUCCESS(goal_id)

    def progress(self, goal_id, goal):
        return 1.0 - self.__progress
    
    def on_effect_in_water(self):
        self.__init_dist = None
        self.__color_goal = None
        self.__progress = 1.0

    def on_effect_moving(self):
        pass

    def on_effect_idle(self):
        self.__init_dist = None
        self.__color_goal = None
        self.__progress = 1.0
    

''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = DiveSkillManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
