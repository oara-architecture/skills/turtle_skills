#!/usr/bin/env python3
import rclpy

from turtle_managers.turn_sensor_on_abstract_manager import AbstractTurnSensorOnSkillManager

from functools import partial
from turtlesim.srv import SetPen

class TurnCameraOnSkillManager(AbstractTurnSensorOnSkillManager):

    def __init__(self):
        super().__init__()
        self.__client = self.create_client(SetPen, 'set_pen')

    def validate(self, goal):
        return self.__client.wait_for_service(timeout_sec=1.0)

    def on_dispatch(self, goal_id, goal):
        self.__future = self.__client.call_async(SetPen.Request(r=250, width=5))
        self.get_logger().info("Turning sensor ON...")
        self.__future.add_done_callback(partial(self.__done_cb, goal_id))

    def __done_cb(self, goal_id, future):
        self.get_logger().info("Sensor ON!")
        self.terminated_ON(goal_id)

    def on_interrupt(self, goal_id, goal):
        self.terminated_ON(goal_id)

    def progress(self, goal_id, goal):
        return .5

    def on_effect_sensor_on(self):
        pass
    

''' Don't change the main function unless you know what you do '''
def main(args=None):
    rclpy.init(args=args)
    server = TurnCameraOnSkillManager()
    server.get_logger().info("Manager created")
    server.spin()

if __name__ == '__main__':
    main()
