#include "rclcpp/rclcpp.hpp"

#include "turtle_managers/srv/get_position_data.hpp"

#include "turtlesim/msg/pose.hpp"


using std::placeholders::_1;
using std::placeholders::_2;

class TurtleDataManager : public rclcpp::Node {
private:
  // position data
  ::turtlesim::msg::Pose position_;
  bool position_received_;
  rclcpp::Subscription<::turtlesim::msg::Pose>::SharedPtr position_subscriber_;
  rclcpp::Service<turtle_managers::srv::GetPositionData>::SharedPtr position_service_;
  
  
  void position_callback(const ::turtlesim::msg::Pose::SharedPtr msg)
  {
    RCLCPP_DEBUG(rclcpp::get_logger("data_manager"), "received data position");
    this->position_received_ = true;
    this->position_ = *msg;
  }

  bool get_position_callback(const std::shared_ptr<turtle_managers::srv::GetPositionData::Request> request,
    std::shared_ptr<turtle_managers::srv::GetPositionData::Response> response)
  {
    (void)request; // remove unused warning
    if (this->position_received_) {
      response->data = this->position_;
      return true;
    }
    else return false;
  }
  

public:
  explicit TurtleDataManager() 
    : rclcpp::Node("data_manager")
  {
    
    this->position_received_ = false;
    this->position_subscriber_ = this->create_subscription<::turtlesim::msg::Pose>(
      "data/position", 1, 
      std::bind(&TurtleDataManager::position_callback, this, _1));
    this->position_service_ =  this->create_service<turtle_managers::srv::GetPositionData>("data/get_position",
      std::bind(&TurtleDataManager::get_position_callback, this, _1, _2));
    
  }

};

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);
  auto manager = std::make_shared<TurtleDataManager>();
  RCLCPP_DEBUG(rclcpp::get_logger("data_manager"), "Data Manager ready");
  rclcpp::spin(manager);
  rclcpp::shutdown();
  return 0;
}
