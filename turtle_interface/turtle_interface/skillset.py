import rclpy

from .dataset_interface import DataSet
from .resourceset_interface import ResourceSet
from .skillsset_interface import SkillsSet

class SkillsetInterface:
    """Skillset Interface for skillset turtle
    """

    def __init__(self, **kwargs):
        self.__name = 'turtle'
        self.__data = DataSet(**kwargs)
        self.__resources = ResourceSet(**kwargs)
        self.__skills = SkillsSet(**kwargs)

    @property
    def data(self) -> DataSet:
        """Get clients to skillset data."""
        return self.__data

    @property
    def resources(self) -> ResourceSet:
        """Get clients to skillset resources."""
        return self.__resources

    @property
    def skills(self) -> SkillsSet:
        """Get clients to skillset skills."""
        return self.__skills
