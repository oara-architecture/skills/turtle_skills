import rclpy

from .resource_interface import *


class ResourceSet:
    def __init__(self, **kwargs):
        self.__resources = []
        
        self.__turtle_status = TurtleStatusResource(**kwargs)
        self.__resources.append(self.__turtle_status)
        self.__sensor = SensorResource(**kwargs)
        self.__resources.append(self.__sensor)
        self.__autonomous_control = AutonomousControlResource(**kwargs)
        self.__resources.append(self.__autonomous_control)
        self.__control_mode = ControlModeResource(**kwargs)
        self.__resources.append(self.__control_mode)

    
    @property
    def turtle_status(self) -> TurtleStatusResource:
        return self.__turtle_status
    
    @property
    def sensor(self) -> SensorResource:
        return self.__sensor
    
    @property
    def autonomous_control(self) -> AutonomousControlResource:
        return self.__autonomous_control
    
    @property
    def control_mode(self) -> ControlModeResource:
        return self.__control_mode
    

    def __getitem__(self, item):
        return getattr(self, item)

    def __iter__(self):
        return iter(self.__resources)

    def __len__(self):
        return len(self.__resources)

    def __nonzero__(self):
        return len(self.__resources) > 0

    def print_states(self):
        for r in self.__resources:
            r.print_state()
