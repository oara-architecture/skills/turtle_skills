import os
from typing import Optional
from threading import Thread
from queue import SimpleQueue
import rclpy
from rclpy.executors import SingleThreadedExecutor
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup

from turtle_managers.srv import State, Transition


class TurtleStatusResource:
    def __init__(self, **kwargs):
        self.__name = 'turtle_status'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__cbgroup = MutuallyExclusiveCallbackGroup()
        self.__transition_client = self.__node.create_client(Transition, f'resource/change_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__state_client = self.__node.create_client(State, f'resource/get_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__queue = SimpleQueue()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self) -> str:
        return self.__name

    def ready(self) -> bool:
        return ( self.__transition_client.service_is_ready()
            and self.__state_client.service_is_ready() )

    def __nonzero__(self):
        return self.ready()

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Resource {self.__name} wait for manager server")
        return self.__state_client.wait_for_service(timeout_sec=timeout)

    def __done_cb(self, future):
        self.__node.get_logger().debug("state received")
        self.__queue.put(future.result().state)

    def get(self) -> str:
        future = self.__state_client.call_async(State.Request())
        future.add_done_callback(self.__done_cb)
        self.__node.get_logger().debug(f"get resource state {self.__name}")
        state = self.__queue.get()
        self.__node.get_logger().debug(f"-- state: {state}")
        return state

    def __eq__(self, state: str):
        return state == self.get()

    def to(self, state: str) -> bool:
        result = self.__transition_client.call(Transition.Request(target=state))
        return result.success

    def __call__(self, tgt = None):
        if tgt is None:
            return self.get()
        else:
            return self.to(tgt)

    def print_state(self):
        self.__node.get_logger().info(f"resource {self.name} in state {self.get()}")

class SensorResource:
    def __init__(self, **kwargs):
        self.__name = 'sensor'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__cbgroup = MutuallyExclusiveCallbackGroup()
        self.__transition_client = self.__node.create_client(Transition, f'resource/change_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__state_client = self.__node.create_client(State, f'resource/get_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__queue = SimpleQueue()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self) -> str:
        return self.__name

    def ready(self) -> bool:
        return ( self.__transition_client.service_is_ready()
            and self.__state_client.service_is_ready() )

    def __nonzero__(self):
        return self.ready()

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Resource {self.__name} wait for manager server")
        return self.__state_client.wait_for_service(timeout_sec=timeout)

    def __done_cb(self, future):
        self.__node.get_logger().debug("state received")
        self.__queue.put(future.result().state)

    def get(self) -> str:
        future = self.__state_client.call_async(State.Request())
        future.add_done_callback(self.__done_cb)
        self.__node.get_logger().debug(f"get resource state {self.__name}")
        state = self.__queue.get()
        self.__node.get_logger().debug(f"-- state: {state}")
        return state

    def __eq__(self, state: str):
        return state == self.get()

    def to(self, state: str) -> bool:
        result = self.__transition_client.call(Transition.Request(target=state))
        return result.success

    def __call__(self, tgt = None):
        if tgt is None:
            return self.get()
        else:
            return self.to(tgt)

    def print_state(self):
        self.__node.get_logger().info(f"resource {self.name} in state {self.get()}")

class AutonomousControlResource:
    def __init__(self, **kwargs):
        self.__name = 'autonomous_control'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__cbgroup = MutuallyExclusiveCallbackGroup()
        self.__transition_client = self.__node.create_client(Transition, f'resource/change_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__state_client = self.__node.create_client(State, f'resource/get_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__queue = SimpleQueue()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self) -> str:
        return self.__name

    def ready(self) -> bool:
        return ( self.__transition_client.service_is_ready()
            and self.__state_client.service_is_ready() )

    def __nonzero__(self):
        return self.ready()

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Resource {self.__name} wait for manager server")
        return self.__state_client.wait_for_service(timeout_sec=timeout)

    def __done_cb(self, future):
        self.__node.get_logger().debug("state received")
        self.__queue.put(future.result().state)

    def get(self) -> str:
        future = self.__state_client.call_async(State.Request())
        future.add_done_callback(self.__done_cb)
        self.__node.get_logger().debug(f"get resource state {self.__name}")
        state = self.__queue.get()
        self.__node.get_logger().debug(f"-- state: {state}")
        return state

    def __eq__(self, state: str):
        return state == self.get()

    def to(self, state: str) -> bool:
        result = self.__transition_client.call(Transition.Request(target=state))
        return result.success

    def __call__(self, tgt = None):
        if tgt is None:
            return self.get()
        else:
            return self.to(tgt)

    def print_state(self):
        self.__node.get_logger().info(f"resource {self.name} in state {self.get()}")

class ControlModeResource:
    def __init__(self, **kwargs):
        self.__name = 'control_mode'
        self.__node = rclpy.create_node(f"turtle_interface_{self.__name}_{os.getpid()}", use_global_arguments=False, **kwargs)
        self.__executor = SingleThreadedExecutor()
        self.__executor.add_node(self.__node)
        self.__cbgroup = MutuallyExclusiveCallbackGroup()
        self.__transition_client = self.__node.create_client(Transition, f'resource/change_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__state_client = self.__node.create_client(State, f'resource/get_{ self.__name }',
                                           callback_group=self.__cbgroup)
        self.__queue = SimpleQueue()
        self.__thread = Thread(target=self.__spin)
        self.__thread.start()

    def __del__(self):
        self.__executor.shutdown()
        self.__thread.join()

    def __spin(self):
        while rclpy.ok():
            try:
                self.__executor.spin_once(timeout_sec=0.1)
            except Exception as e:
                return
    
    @property
    def name(self) -> str:
        return self.__name

    def ready(self) -> bool:
        return ( self.__transition_client.service_is_ready()
            and self.__state_client.service_is_ready() )

    def __nonzero__(self):
        return self.ready()

    def wait_manager(self, timeout: Optional[float] = None) -> bool:
        self.__node.get_logger().debug(f"Resource {self.__name} wait for manager server")
        return self.__state_client.wait_for_service(timeout_sec=timeout)

    def __done_cb(self, future):
        self.__node.get_logger().debug("state received")
        self.__queue.put(future.result().state)

    def get(self) -> str:
        future = self.__state_client.call_async(State.Request())
        future.add_done_callback(self.__done_cb)
        self.__node.get_logger().debug(f"get resource state {self.__name}")
        state = self.__queue.get()
        self.__node.get_logger().debug(f"-- state: {state}")
        return state

    def __eq__(self, state: str):
        return state == self.get()

    def to(self, state: str) -> bool:
        result = self.__transition_client.call(Transition.Request(target=state))
        return result.success

    def __call__(self, tgt = None):
        if tgt is None:
            return self.get()
        else:
            return self.to(tgt)

    def print_state(self):
        self.__node.get_logger().info(f"resource {self.name} in state {self.get()}")

