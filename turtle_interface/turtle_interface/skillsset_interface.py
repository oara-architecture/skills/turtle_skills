import rclpy

from .skill_interface import *


class SkillsSet:

    def __init__(self, **kwargs):
        self.__skills = []
        
        self.__dive = DiveSkillInterface(**kwargs)
        self.__skills.append(self.__dive)
        self.__ascend = AscendSkillInterface(**kwargs)
        self.__skills.append(self.__ascend)
        self.__move_on_surface = MoveOnSurfaceSkillInterface(**kwargs)
        self.__skills.append(self.__move_on_surface)
        self.__move = MoveSkillInterface(**kwargs)
        self.__skills.append(self.__move)
        self.__turn_sensor_on = TurnSensorOnSkillInterface(**kwargs)
        self.__skills.append(self.__turn_sensor_on)
        self.__turn_sensor_off = TurnSensorOffSkillInterface(**kwargs)
        self.__skills.append(self.__turn_sensor_off)
        self.__search = SearchSkillInterface(**kwargs)
        self.__skills.append(self.__search)

    
    @property
    def dive(self) -> DiveSkillInterface:
        return self.__dive
    
    @property
    def ascend(self) -> AscendSkillInterface:
        return self.__ascend
    
    @property
    def move_on_surface(self) -> MoveOnSurfaceSkillInterface:
        return self.__move_on_surface
    
    @property
    def move(self) -> MoveSkillInterface:
        return self.__move
    
    @property
    def turn_sensor_on(self) -> TurnSensorOnSkillInterface:
        return self.__turn_sensor_on
    
    @property
    def turn_sensor_off(self) -> TurnSensorOffSkillInterface:
        return self.__turn_sensor_off
    
    @property
    def search(self) -> SearchSkillInterface:
        return self.__search
    

    def __len__(self):
        return len(self.__skills)

    def __nonzero__(self):
        return len(self.__skills) > 0

    def __iter__(self):
        return iter(self.__skills)

    def __getitem__(self, item):
        return getattr(self, item)
